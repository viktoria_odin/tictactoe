#pragma once

#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe
{
private:
	int numTurns;
	char playerTurn;
	char winner;
	char board[9];

public:
	//constructors
	TicTacToe()
	{
		for (int i = 0; i < 9; i++)
		{
			board[i] = ' ';
		}
		numTurns = 0;
		playerTurn = 'X';
		winner = ' ';
	}


	//methods
	virtual void DisplayBoard()
	{
		cout << " " << board[0] << " | " << board[1] << " | " << board[2] ;
		cout << "\n " << board[3] << " | " << board[4] << " | " << board[5];
		cout << "\n " << board[6] << " | " << board[7] << " | " << board[8] << "\n";
	}

	virtual bool IsOver()
	{
		bool result;

		if (numTurns == 9)
		{
			result = true;
		}
		else
		{
			if (board[0] == board[1] && board[1] == board[2] && board[0] != ' ')
			{
				winner = board[0];
			}
			else if (board[3] == board[4] && board[4] == board[5] && board[3] != ' ')
			{
				winner = board[3];
			}
			else if (board[6] == board[7] && board[7] == board[8] && board[6] != ' ')
			{
				winner = board[6];
			}
			else if (board[0] == board[3] && board[3] == board[6] && board[0] != ' ')
			{
				winner = board[0];
			}
			else if (board[1] == board[4] && board[4] == board[7] && board[1] != ' ')
			{
				winner = board[1];
			}
			else if (board[2] == board[5] && board[5] == board[8] && board[2] != ' ')
			{
				winner = board[2];
			}
			else if (board[0] == board[4] && board[4] == board[8] && board[0] != ' ')
			{
				winner = board[0];
			}
			else if (board[2] == board[4] && board[4] == board[6] && board[2] != ' ')
			{
				winner = board[2];
			}

			if (winner == ' ')
			{
				result = false;
			}
			else
			{
				result = true;
			}
		}
		return result;

	}

	virtual char GetPlayerTurn()
	{
		return playerTurn;
	}

	virtual bool IsValidMove(int position)
	{
		if (position > 0 && position < 10)
		{
			if (board[position - 1] == ' ')
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else {
			return false;
		}
	}

	virtual void Move(int position)
	{
		board[position - 1] = playerTurn;

		if (playerTurn == 'X')
		{
			playerTurn = 'O';
		}
		else
		{
			playerTurn = 'X';
		}
		numTurns += 1;

	}

	virtual void DisplayResult()
	{
		if (winner == ' ')
		{
			cout << "The game ended with a tie!\n";
		}
		else
		{
			cout << "The winner is " << winner << "!\n";
		}
	}
};
